Vue.component('forms',{
    template: "#formulario",
    data(){
        return {
            nome: '',
        }
    },
    methods:{
        setCidade(){
            eventBus.$emit('setCidade', this.nome)
        },
        filtre(){
            eventBus.$emit('filtre', this.nome)
        },

        
    }
})

Vue.component('tupla-cidades',{
    template: "#tupla",
    
    created(){
        var vm = this;
        eventBus.$on('setCidade',function(nome){
            var url = 'https://api.openweathermap.org/data/2.5/weather?q='+nome+'&appid=77d874a0e278cc9f9b09a9324e2ce197&lang=pt_br&units=metric';
            this.$http.get(url,{}).then(function (response) {
                vm.listo.push(response.body);
                vm.boolean = true;
                console.log(this.boolean);
            }, function (erro) {
                console.log("ERRO");
                alert("Erro! A pesquisa por: "+nome+" não encontrada.")
            });
        });
        eventBus.$on('filtre',function(nome){
            vm.filtro = nome;
            
        })
    },
    computed:{
        
        procuraCidades: function(){          
            var k = this.listo.filter((cid) => cid.name.toUpperCase().includes(this.filtro.toUpperCase()),);
            return k
        }
    },
    
    methods:{
        remover(indice){
            this.listo.splice(indice,1);
        }
    },
    data(){
        return{
            listo:[],
            filtro:"",
            len : 0,
            boolean : false,
            path:'https://openweathermap.org/img/wn/',
            ext:'.png',
        }
    }
});


var eventBus = new Vue({});

var app = new Vue({
    el: ".app",
    data: {
        titulo: "Explore Clima",
        name:''
        
    },
    methods:{
       
    }
});    
